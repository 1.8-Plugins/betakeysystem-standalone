package de.unbemerkt.keyplugin.MySQL;

import java.sql.*;

public class MySQLLogin {

    private final String HOST;
    private final String DATABASE;
    private final String USER;
    private final String PASSWORD;

    private Connection con;

    public MySQLLogin(String host, String database, String user, String password) {
        HOST = host;
        DATABASE = database;
        USER = user;
        PASSWORD = password;

        connect();
    }

    public void connect() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + HOST + ":3306/" + DATABASE + "?autoReconnect=true&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    USER, PASSWORD);
            System.out.println("[Key] Die Verbindung zum Login-Server wurde hergestellt!");
        } catch (SQLException e) {
            System.out.println("[Key] Die Verbindung zum Login-Server ist fehlgeschlagen! Fehler: " + e.getMessage());
        }
    }

    public void close() {
        try {
            if (con != null) {
                con.close();
                System.out.println("[Key] Die Verbindung zum Login-Server wurde Erfolgreich beendet!");
            }
        } catch (SQLException e) {
            System.out.println("[Key] Fehler beim beenden der Verbindung zum Login-Server! Fehler: " + e.getMessage());
        }
    }

    public void update(String qry) {
        Statement st = null;
        try {
            st = con.createStatement();
            st.executeUpdate(qry);
        } catch (Exception e) {
            connect();
            System.err.println(e);
        }
        closeStatement(st);
    }

    public ResultSet query(String qry) {
        ResultSet rs = null;
        try {
            Statement st = con.createStatement();
            rs = st.executeQuery(qry);
        } catch (SQLException e) {
            connect();
            System.err.println(e);
        }
        return rs;
    }

    public static void closeStatement(Statement st){
        if(st != null)
            try {
                st.close();
            }catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static void closeResultset(ResultSet rs){
        if(rs != null)
            try {
                rs.close();
            }catch (Exception e) {
                e.printStackTrace();
            }
    }





}
