package de.unbemerkt.keyplugin.Main;


import de.unbemerkt.keyplugin.LISTENER.JOIN;
import de.unbemerkt.keyplugin.MySQL.MySQL;
import de.unbemerkt.keyplugin.MySQL.MySQLLogin;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static de.unbemerkt.keyplugin.UTILS.md5.getMD5;


public class Main extends JavaPlugin {



    public static String Username= "";
    public static String Password= "";
    public static String ServerKey= "";


    public static String HOST = "";
    public static String USER = "";
    public static String PASSWORD = "";
    public static String DB = "";

    public static String HOST_LOGIN = "";
    public static String USER_LOGIN = "";
    public static String PASSWORD_LOGIN = "";
    public static String DB_LOGIN = "";


    public static MySQL mysql;
    public static MySQLLogin mysqllogin;


    public static Main plugin;
    public static String prefix = "§bKey §8| §7";
    public static String redprefix = "§cKey §4| §c";
    public boolean login = false;

    @Override
    public void onEnable() {
        plugin = this;

        HOST = getConfig().getString(".MySQL.Server.Host");
        USER = getConfig().getString(".MySQL.Server.User");
        PASSWORD = getConfig().getString(".MySQL.Server.Password");
        DB = getConfig().getString(".MySQL.Server.DB");

        HOST_LOGIN = getConfig().getString(".MySQL.Login.Host");
        USER_LOGIN = getConfig().getString(".MySQL.Login.User");
        PASSWORD_LOGIN = getConfig().getString(".MySQL.Login.Password");
        DB_LOGIN = getConfig().getString(".MySQL.Login.DB");

        Username = getConfig().getString(".Login.Username");
        Password = getConfig().getString(".Login.Password");
        ServerKey = getConfig().getString(".ServerKey");
        getConfig().options().copyDefaults(true);
        saveConfig();
        try {
            ConnectMySQL();

        } catch (IOException e) {
            e.printStackTrace();
            login = false;
            return;
        }
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        registerCommands();
        registerEvents();
        if (!Username.equalsIgnoreCase("USER") || !Password.equalsIgnoreCase("PW")){
            if (login(Password,Username)){
                login = true;
                console.sendMessage(prefix+"Erfolgreich beim Server Angemeldet");
                if(existServer()){
                    console.sendMessage(prefix+"Server Gefunden und Owner Bestätigt!");
                }else {
                    console.sendMessage(redprefix+"Server nicht Gefunden!");
                }
            }else{
                console.sendMessage(redprefix+"Konnte nicht beim Server Anmelden!");
                login = false;
                //getServer().getPluginManager().disablePlugin(plugin);
                return;
            }
            console.sendMessage(prefix+"Plugin Erfolgreich Geladen!");
            try {
                Bukkit.getScheduler().runTaskLater(this, new Runnable() {
                    @Override
                    public void run() {
                        setState("ONLINE");
                    }
                },3*20);
                startResetCount();

            }catch (Exception e){

            }

        }else{
            login = false;
            console.sendMessage(redprefix+"Login Fehlgeschlagen!");
            //getServer().getPluginManager().disablePlugin(plugin);

        }

    }
    public static void ConnectMySQL() throws IOException {
        mysql = new MySQL(HOST, DB,
                USER, PASSWORD);
        mysql.update("CREATE TABLE IF NOT EXISTS `GROUPS`(id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, OWNER TEXT, NAME TEXT, GROUP_KEY TEXT)");
        mysql.update("CREATE TABLE IF NOT EXISTS `SERVER_KEYS`(id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, S_KEY TEXT, SERVER_KEY TEXT, USED TEXT,ip TEXT)");
        mysql.update("CREATE TABLE IF NOT EXISTS `SERVER_LOGIN`(id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, playername TEXT, UUID TEXT, FIRST_JOIN TEXT, SERVER_KEY TEXT, SERVER_GROUP TEXT)");
        mysql.update("CREATE TABLE IF NOT EXISTS `SERVER`(id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, SERVER_KEY TEXT, NAME TEXT, NAME_KEY TEXT, OWNER TEXT, IP TEXT, ONLINE_STATE TEXT, GROUP_USE TEXT, GROUP_NAME TEXT, COUNT_CHECK_ONLINE_STATE int(11))");
        mysqllogin = new MySQLLogin(HOST_LOGIN, DB_LOGIN,
                USER_LOGIN, PASSWORD_LOGIN);
        mysqllogin.update("CREATE TABLE IF NOT EXISTS `LOGIN`(id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, name TEXT, password TEXT, email TEXT)");

    }
    public static void setPassword(String password) {
        Password = password;
    }

    public static void setServerKey(String serverKey) {
        ServerKey = serverKey;
    }

    public static void setUsername(String username) {
        Username = username;
    }

    @Override
    public void onDisable() {
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        if (login){
            try {

                setState("OFFLINE");
            }catch (Exception e){

            }
        }



                console.sendMessage(redprefix+"Plugin Erfolgreich Entladen!");


    }
    public void registerEvents(){
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new JOIN(), this);
    }

    public void registerCommands(){

    }


    private boolean setState(String state) {
        try {
            if(state.equalsIgnoreCase("OFFLINE")) {
                mysql.update("UPDATE `SERVER` SET `ONLINE_STATE` = '"+state+"',`COUNT_CHECK_ONLINE_STATE`='10' WHERE `SERVER_KEY`='"+ServerKey+"' AND `OWNER`='"+Username+"'");

            }else {
                mysql.update("UPDATE `SERVER` SET `ONLINE_STATE` = '"+state+"',`COUNT_CHECK_ONLINE_STATE`='0' WHERE `SERVER_KEY`='"+ServerKey+"' AND `OWNER`='"+Username+"'");

            }
            return true;
        }catch(Exception e){
            return false;
        }


    }


    public static boolean Join(String name,String UUID) {
        if(!inGroup()) {
            if(!getPlayer(name,UUID)) {
                return false;
            }else {
                return true;
            }

        }else if(getGroup().equalsIgnoreCase("FREE4ALL")) {
            return true;
        }else {
            if(!getPlayer2(name,UUID)) {
                return false;
            }else {
                return true;
            }
        }
    }

    public static boolean getPlayer(String name, String UUID){
        String url = "jdbc:mysql://"+HOST+":3306/"+DB+"?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        String query = "SELECT * FROM `SERVER_LOGIN` WHERE `playername`='"+name+"' AND (`UUID`='"+UUID+"' OR `FIRST_JOIN`='true') AND `SERVER_KEY`='"+ServerKey+"'";
        try (Connection con = DriverManager.getConnection(url, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(query);
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                if (rs.getString(4).equalsIgnoreCase("true")){
                    updateUUID(UUID,name);
                }
                return true;
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MySQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }

    public static void updateUUID(String UUID, String Name){
        mysql.update("UPDATE `SERVER_LOGIN` SET `FIRST_JOIN`='false', `UUID`='"+UUID+"' WHERE `playername`='"+Name+"' AND (`SERVER_KEY`='"+ServerKey+"' OR `SERVER_GROUP`='"+getGroup()+"')");
    }
    public static boolean getPlayer2(String name, String UUID){
        String url = "jdbc:mysql://"+HOST+":3306/"+DB+"?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        String query = "SELECT * FROM `SERVER_LOGIN` WHERE `playername`='"+name+"' AND (`UUID`='"+UUID+"' OR `FIRST_JOIN`='true') AND (`SERVER_KEY`='"+ServerKey+"' OR `SERVER_GROUP`='"+getGroup()+"')";
        try (Connection con = DriverManager.getConnection(url, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(query);
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                if (rs.getString(4).equalsIgnoreCase("true")){
                    updateUUID(UUID,name);
                }
                return true;
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MySQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }

    public void startResetCount(){
        if (!login)return;
            Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                setState("ONLINE");
                startResetCount();
            }
        },20*20);
    }

    public boolean existServer(){
        String url = "jdbc:mysql://"+HOST+":3306/"+DB+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        String query = "SELECT * FROM `SERVER` WHERE `SERVER_KEY`='"+ServerKey+"' AND OWNER='"+Username+"'";
        try (Connection con = DriverManager.getConnection(url, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(query);
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MySQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;

    }



    public boolean login(String password, String username){
        String url = "jdbc:mysql://"+HOST_LOGIN+":3306/"+DB_LOGIN+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        String query = "SELECT * FROM `LOGIN` WHERE `name`='"+username+"' AND password='"+getMD5(password)+"'";
        try (Connection con = DriverManager.getConnection(url, USER_LOGIN, PASSWORD_LOGIN);
             PreparedStatement pst = con.prepareStatement(query);
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MySQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;

    }

    public static String getGroup(){
        if (inGroup()){
            String url = "jdbc:mysql://"+HOST+":3306/"+DB+"?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

            String query = "SELECT * FROM `SERVER` WHERE `SERVER_KEY`='"+ServerKey+"' AND `GROUP_USE`='Ja'";
            try (Connection con = DriverManager.getConnection(url, USER, PASSWORD);
                 PreparedStatement pst = con.prepareStatement(query);
                 ResultSet rs = pst.executeQuery()) {
                while (rs.next()) {
                    return rs.getString(9);
                }
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(MySQL.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        return "none";
    }


    public static boolean inGroup(){
        String url = "jdbc:mysql://"+HOST+":3306/"+DB+"?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        String query = "SELECT * FROM `SERVER` WHERE `SERVER_KEY`='"+ServerKey+"' AND `GROUP_USE`='Ja'";
        try (Connection con = DriverManager.getConnection(url, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(query);
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MySQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }


    public boolean isLogin() {
        return login;
    }


    public void setLogin(boolean login) {
        this.login = login;
    }

    public static String replacer(String string){
        return string.replace("<<","«")
                .replace(">>","»")
                .replace("&uuml;","ü")
                .replace("&uaml;","ä")
                .replace("&uoml;","ö")
                .replace("&","§");
    }
}
